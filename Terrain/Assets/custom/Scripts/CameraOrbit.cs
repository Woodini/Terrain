﻿using UnityEngine;

public class CameraOrbit : MonoBehaviour {
	[Tooltip("Whether or not you need to right-click your mouse to orbit the camera.")]
	public bool requiresRightClick = true;
	[Tooltip("The transform target that the camera considers to be the 'centre of rotation'.")]
	public Transform target;
	[Tooltip("the speed at which the mouse zooms in and out upon mouse-scroll.")]
	public float zoomSpeed = 20.0f;
	[Tooltip("The default zoom value of the camera. Ideally, it's kept within the lower and upper bounds of the zoomDistanceMinMax property.")]
	public float zoomDistance = 12.0f;
	[Tooltip("The allowed lower and upper bounds that limit the camera's zoom.")]
	public Vector2 zoomDistanceMinMax = new Vector2(7.0f, 15.0f);
	[Tooltip("The height of the camera in relation to the target transform.")]
	public float cameraHeight = 2.0f;
	[Tooltip("The speed of the orbit.")]
	public Vector2 speed = new Vector2(250.0f, 120.0f);
	[Tooltip("The minimum and maximum Y value for the camera. This will prevent it from orbiting too high above the character, or too low in to the ground.")]
	public Vector2 minMaxYLimit = new Vector2(1.0f, 75.0f);

	private Vector2 xy = Vector2.zero;
	private Quaternion last_rotation;

	void Start () {
		Vector3 angles = transform.eulerAngles;
		xy.x = angles.y;
		xy.y = angles.x;

		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>()) {
			GetComponent<Rigidbody>().freezeRotation = true;
		}

		if (!requiresRightClick) {
			GetCameraStartAngle();
		}
	}

	void GetCameraStartAngle() {
		// The Unity script inverses the x and y.
		// Also, start adding the mouse rotation to the current transform Euler Angles
		xy.x = transform.eulerAngles.y;
		xy.y = ClampAngle(transform.eulerAngles.x, minMaxYLimit.x, minMaxYLimit.y);
	}

	void RotateWithMouse() {
		// Add mouse values to rotation, based on the previous camera rotation
		xy.x += Input.GetAxis("Mouse X") * speed.x * 0.02f;
		xy.y -= Input.GetAxis("Mouse Y") * speed.y * 0.02f;

		// Clamp the y so weird things don't happen
		xy.y = ClampAngle(xy.y, minMaxYLimit.x, minMaxYLimit.y);

		Quaternion rotation = Quaternion.Euler(xy.y, xy.x, 0);
		Vector3 position = rotation * new Vector3(0.0f, cameraHeight, -zoomDistance) + target.position;

		transform.position = position;
		transform.rotation = rotation;

		last_rotation = rotation;
	}

	void LateUpdate () {
		if (target) {
			// Scrollwheel to set camera distance
			zoomDistance = Mathf.Clamp(zoomDistance - Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, zoomDistanceMinMax.x, zoomDistanceMinMax.y);

			if (Input.GetButtonDown("Fire2") && requiresRightClick) {
				GetCameraStartAngle();
			}

			if (Input.GetButton("Fire2") && requiresRightClick) {
				RotateWithMouse();
			} else if (requiresRightClick) {
				//Follow player even when not pressing 3rd mouse button
				transform.position = last_rotation * new Vector3(0.0f, cameraHeight, -zoomDistance) + target.position;
				transform.rotation = last_rotation;
			} else {
				RotateWithMouse();
			}
		}
	}

	private float ClampAngle(float angle, float min, float max) {
		if (angle < -360) {
			angle += 360;
		}

		if (angle > 360) {
			angle -= 360;
		}

		// Hacked to get it working with the Euler angles. Could be better I guess...
		if (angle > 360 + min) {
			//angle = 180 -angle;
			return angle;
		}

		if (angle > 180) {
			angle = -angle;
		}

		return Mathf.Clamp(angle, min, max);
	}
}
